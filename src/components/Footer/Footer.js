import React from "react";
import './Footer.css';

function Footer() {

    return(
        <div className="footer">
            <div className="container footer-copy">Movie Search Engine. Copyright 2020.</div>
        </div>
    );
};

export default Footer;